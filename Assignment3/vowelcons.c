#include <stdio.h>

int main()
{ 
	char le;
	printf("Enter: ");
	scanf("%c", &le);
	if (le=='a' || le=='e' || le=='i' || le=='o' || le=='u' || le=='A' || le=='E' || le=='I' || le=='O' || le=='U')
		printf("%c is a vowel", le);
	else
		printf("%c is a consonant", le);
	return 0;
}