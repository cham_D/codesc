#include <stdio.h>

struct studentinfo{
    char name[20];
    char subject[20];
    int marks;
};

int main()
{
    struct studentinfo student[5];
	printf("Enter student details;\n");
    for(int i=0; i<5; i++){
        printf("Enter name - ");
        scanf("%s", &student[i].name);
        printf("Enter subject - ");
        scanf("%s", &student[i].subject);
        printf("Enter marks - ");
        scanf("%d", &student[i].marks);
		printf("\n");
    }
	printf("The entered student details;\n");
    for(int i=0; i<5; i++){
        printf("\nname - %s\nsubject - %s\nmarks - %d\n", student[i].name, student[i].subject, student[i].marks);
    }
    return 0;
}
