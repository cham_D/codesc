#include <stdio.h>

#define MAXSIZE 100

void ch_frequen(char [], char s);

int main(){
	char str[MAXSIZE];
	char s;
	int i=0;
	printf("Sentence: ");
	fgets(str, MAXSIZE, stdin);
	printf("Enter a  character: ");
	scanf("%c", &s);
	printf("The frequency of %c is: ", s);
	ch_frequen(str, s);
	return 0;
}

void ch_frequen(char str[], char s){
	int N, i=0, count=0;
	while (str[i] != '\0'){
		if(str[i] == s){
			++count;
		}
		++i;
	}
	printf("%d", count);
}


