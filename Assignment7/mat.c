#include <stdio.h>

int main(){
	int i, n, x, y, z, sum = 0, add=0;
	int a[100][100], b[100][100], multi[100][100], addi[100][100], arow, acol, brow, bcol;
	
	printf("Enter the no.of rows and columns for the 1st matrix: ");
	scanf("%d %d", &arow, &acol);
	printf("Enter the no.of rows and columns for the 2nd matrix: ");
	scanf("%d %d", &brow, &bcol);
	
	printf("Enter values for matrix 1: ");
	for(i = 0; i < arow; i++){
		for(n = 0; n < acol; n++){
			scanf("%d", &a[i][n]);
		}
	}
	printf("Enter values for matrix 2: ");
	for(i = 0; i < brow; i++){
		for(n = 0; n < bcol; n++){
			scanf("%d", &b[i][n]);
		}
	}
	printf("Matrices Addition:\n");
	for(i=0; i<arow; i++){
		for(n=0; n<acol; n++){
			addi[i][n]= a[i][n] + b[i][n];
			printf("%d   ", addi[i][n]);
		}
		printf("\n");
	}
	
	for(x = 0; x < arow; x++){
		for(y = 0; y < bcol; y++){
			for(z = 0; z < brow; z++){
				sum = sum + a[x][z]*b[z][y];
				
			}
			multi[x][y] = sum;
			sum = 0;
		}
	}
	printf("Matrices Multiplication:\n");
	
	for(i = 0; i < arow; i++){
		for(n = 0; n < bcol; n++){
			printf("%d     ", multi[i][n]);
			
		}
		printf("\n");
	}
	return 0;
}
